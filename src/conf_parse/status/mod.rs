use crate::conf_parse::rules::Rules;
use std::str::Chars;
use crate::conf_parse::expression::Expression;

#[derive(Clone, Debug)]
pub struct Possition{
    pub n: usize,
}

#[derive(Clone, Debug)]
pub struct Status<'a>{
    pub desc: &'a str,
    pub chars: Chars<'a>,
    pub pos: Possition,
}

impl Possition{
    fn new()->Possition{
        return Possition{
            n: 0,
        };
    }
}

impl <'a> Status<'a>{
    pub fn new(desc: &'a str)->Status<'a>{

        return Status{
            desc: desc,
            chars: desc.chars(),
            pos: Possition::new(),
        };
    }
    pub fn get_char(&mut self)->Option<char>{
        let tmp_char = self.chars.next()?;

        self.pos.n += 1;
        let (_,tmp_desc) = self.desc.split_at(self.pos.n);
        self.chars = tmp_desc.chars();

        return Some(tmp_char);
    }
    pub fn possion_bak(&mut self, pos: usize){
        if pos > self.pos.n{
            error!("possion_bak pos {} > self.pos.n {}", pos, self.pos.n);
            std::process::exit(-1);
        }
        self.pos.n -= pos;
        let (_,tmp_desc) = self.desc.split_at(self.pos.n);
        self.chars = tmp_desc.chars();
    }
    pub fn debug(&self)->String{
        format!("chars [{:?}] pos [{}]", self.chars, self.pos.n)
    }
}