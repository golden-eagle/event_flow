use std::sync::Arc;

//一列数据
pub struct Series(pub Arc<dyn SeriesTrait>);

pub trait SeriesTrait{
    fn rename(&mut self, name: &str);
}