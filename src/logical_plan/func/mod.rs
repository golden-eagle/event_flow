use crate::run_plan::{RunPlan, RunPlanInstance};

pub struct FuncInstance{
    pub condition: Option<Box<RunPlanInstance>>,
    pub func: Box<RunPlanInstance>,
}