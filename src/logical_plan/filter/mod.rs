use crate::run_plan::RunPlanInstance;

pub struct FilterInstance{
    pub func: Box<RunPlanInstance>,
}
