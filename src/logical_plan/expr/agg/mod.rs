use crate::logical_plan::expr::LogicalPlanExpr;

#[derive(Debug,Clone)]
pub enum AggExpr{
    Min(Box<LogicalPlanExpr>),
    Max(Box<LogicalPlanExpr>),
    First(Box<LogicalPlanExpr>),
    Last(Box<LogicalPlanExpr>),
    Count(Box<LogicalPlanExpr>),
    Sum(Box<LogicalPlanExpr>),   //reduce groups to the sum of all values
    Median(Box<LogicalPlanExpr>),//中位数
    NUnique(Box<LogicalPlanExpr>),//nth unique value in a group
    Avg(Box<LogicalPlanExpr>),
    List(Box<LogicalPlanExpr>),//group 序列化成一个list
    AggGroups(Box<LogicalPlanExpr>),//组的组索引
    Std(Box<LogicalPlanExpr>),//数列值的标准差
    Var(Box<LogicalPlanExpr>),//数列值的方差
}