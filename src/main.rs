#![feature(try_trait_v2)]

#[macro_use]
extern crate log;
extern crate log4rs;
extern crate chrono;
extern crate async_channel;
extern crate once_cell;
extern crate async_trait;
#[macro_use]
extern crate clap;
extern crate tokio;
extern crate toml;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
#[macro_use]
extern crate lazy_static;
extern crate idata;
extern crate snowflake;
extern crate serde_yaml;
extern crate async_recursion;
extern crate elasticsearch;
extern crate async_std;
extern crate json;

use crate::logical_plan::logicalplan_map;
use crate::test::func_arg_test;
use crate::conf_init::{conf_map_run, MapResult, EVENT_FLOW_CMD_CONF_INSTANCE};

pub mod test;
pub mod conf_parse;
pub mod conf_init;
pub mod util;
pub mod data_frame;
pub mod logical_plan;
pub mod physical_plan;
pub mod run_plan;
pub mod source;
pub mod sink;



fn main() {

    if let Err(e) = log4rs::init_file(&EVENT_FLOW_CMD_CONF_INSTANCE.log4rs_yaml, Default::default()){
        println!("log4rs init_file failed, {}", e);
        std::process::exit(-1);
    }

    println!("Hello, world!");
    let ret = func_arg_test();
    println!("ret: {:?}", ret);

    if let MapResult::Err(e) = conf_map_run(){
        println!("conf_map_run err {:?}", e);
        return;
    }
}
