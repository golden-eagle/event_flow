/*
时间、uuid等通用处理函数
*/

use snowflake::SnowflakeIdGenerator;
use once_cell::sync::OnceCell;
use std::sync::RwLock;
use crate::conf_init::EventFlowNormalConfInstance;
use crate::data_frame::{DataInstance, RealValue};
use crate::physical_plan::PhysicalPlanExpr;
use crate::run_plan::{HandResult, HandResultOk};

static SNOW_FLAKE: OnceCell<RwLock<SnowflakeIdGenerator>> = OnceCell::new();

pub fn uuid(args: &[Box<PhysicalPlanExpr>], data: &mut DataInstance)->HandResult{
    let tmp = SNOW_FLAKE.get_or_init(||{
        RwLock::new(SnowflakeIdGenerator::new(
            EventFlowNormalConfInstance.snowflake_machine_id as i32,
        EventFlowNormalConfInstance.snowflake_node_id as i32))
    });

    let id = tmp.write().unwrap().generate() as u64;
    return HandResult::Ok(HandResultOk::new(RealValue::U64(id)));
}