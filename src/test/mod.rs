
use crate::logical_plan::expr::conf_func_map_logicalplan_expr;
use std::collections::HashMap;
use crate::logical_plan::{logicalplan_map};
use crate::conf_init::MapResult;

pub fn func_arg_test()->MapResult<()>{
    let pre_define_col_port = "col([a,b,port])";
    let pre_define_col_value = "literal(80)";
    let my_filter1 = "and(<col_port>,80)";
    let my_filter2 = "set(<col_port>,[d,e,port])";
    let my_filter3 = "set([a,b,c],90)";

    let mut pre_define = HashMap::new();
    let col_port = conf_func_map_logicalplan_expr(pre_define_col_port, &pre_define)?;
    let col_value = conf_func_map_logicalplan_expr(pre_define_col_value, &pre_define)?;

    pre_define.insert("col_port".to_string(), col_port);
    pre_define.insert("col_value".to_string(), col_value);

    let filter1 = conf_func_map_logicalplan_expr(my_filter1, &pre_define)?;
    let filter2 = conf_func_map_logicalplan_expr(my_filter2, &pre_define)?;
    let filter3 = conf_func_map_logicalplan_expr(my_filter3, &pre_define)?;

    println!("filter1: {:?}", filter1);
    println!("filter2: {:?}", filter2);
    println!("filter3: {:?}", filter3);

    println!("runplan1: {:?}", logicalplan_map(&filter1));
    println!("runplan2: {:?}", logicalplan_map(&filter2));
    println!("runplan3: {:?}", logicalplan_map(&filter3));

    return MapResult::Ok(());
}
