



use crossbeam_channel::{Sender, Receiver, bounded};
use async_channel::{bounded as async_bounded, Sender as AsyncSender, Receiver as AsyncReceiver};
use crate::data_frame::DataInstance;
use crate::conf_init::EventFlowNormalConfInstance;

#[derive(Clone,Debug)]
pub struct AsyncRecv{
    recv: AsyncReceiver<DataInstance>,
}

#[derive(Clone,Debug)]
pub struct AsyncSend{
    send: AsyncSender<DataInstance>,
}

impl AsyncSend{
    pub async fn send(&self, data: DataInstance){
        match self.send.send(data).await{
            Ok(_) => {},
            Err(e) => {
                error!("AsyncMPMCSend send failed [{:?}]", e);
                std::process::exit(-1);
            }
        }
    }
}

impl AsyncRecv{
    pub async fn recv(&self)->Option<DataInstance>{
        match self.recv.recv().await{
            Ok(t) => {Some(t)},
            Err(e) => {
                error!("AsyncMPMCRecv recv failed [{:?}]", e);
                std::process::exit(-1);
            },
        }
    }
}

pub fn async_channel_new()->(AsyncSend,AsyncRecv){
    let (s,r) = async_bounded(EventFlowNormalConfInstance.node.async_channel_size);
    return (AsyncSend{send: s}, AsyncRecv{recv: r});
}


